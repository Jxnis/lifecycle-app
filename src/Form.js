import React, { Component } from "react";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      globalTitle: "Hello World"
    };
    this.onTitleChange = this.onTitleChange.bind(this);
    this.submitForm = this.submitForm.bind(this);
  }

  componentDidMount() {
    console.log("Rendered Form");
  }

  onTitleChange = e => this.setState({ title: e.target.value });

  submitForm = (e) => { 
      e.preventDefault() 
    this.setState({ globalTitle: "My form - " + this.state.title });
  }

  componentDidUpdate(prevProps) {
    if (this.state.globalTitle !== prevProps.globalTitle) {
      console.log("Title changed");
    }
  }

  render() {
    return (
      <div className="Form">
          <h1>{this.state.globalTitle}</h1>
        <form onSubmit={this.submitForm}>
          <div className="form-data">
            <label htmlFor="url"></label>
            <input
              type="text"
              id="title"
              name="title"
              onChange={this.onTitleChange}
             
            />
          </div>
          <button type="submit">Validate</button>
        </form>
      </div>
    );
  }
}

export default Form;
